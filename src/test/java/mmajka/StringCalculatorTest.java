package mmajka;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringCalculatorTest {

    private StringCalculator sc;

    @Before
    public void init() {
        sc = new StringCalculator();
    }

    @Test
    public void nullNumbers_shouldReturnZero() throws Exception {
        // given
        String numbers = null;

        // when
        int result = sc.add(numbers);

        // then
        int expected = 0;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void emptyString_shouldReturnZero() throws Exception {
        // given
        String numbers = "";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 0;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void singleCommaDelimiterOnly_manyNums_shouldAddAll() throws Exception {
        // given
        String numbers = "1,1,1,1,1,1,1,1,1,1";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 10;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void defaultDelimiters_fiveNumbers_shouldAddAll() throws Exception {
        // given
        String numbers = "1\n1,1,1\n1";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 5;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void customDelimiter_oneNum_shouldAddAll() throws Exception {
        // given
        String numbers = "//;\n1";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 1;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void customDelimiter_manyNums_shouldAddAll() throws Exception {
        // given
        String numbers = "//__\n1__2__3";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 6;
        Assert.assertEquals(expected, result);
    }


    @Test(expected = Exception.class)
    public void customDelimiter_negativeNum_shouldThrowException() throws Exception {
        // given
        String numbers = "//__\n-1";

        // when
        sc.add(numbers);
    }

    @Test(expected = Exception.class)
    public void customDelimiter_negativeAndPositiveNums_shouldThrowException() throws Exception {
        // given
        String numbers = "//,\n-1,1,1,-2,333";

        // when
        sc.add(numbers);
    }

    @Test
    public void customerDelimiter_numbsBiggerThan1000_shouldNotAddNumsAbove1000() throws Exception {
        // given
        String numbers = "//,\n1,2,1000,2000";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 3;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void customerDelimiter_squareBrackets_shouldAddAll() throws Exception {
        // given
        String numbers = "//[k]\n1k2";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 3;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void manyCustomerDelimiters_squareBrackets_shouldAddAll() throws Exception {
        // given
        String numbers = "//[k][m]\n1k2m3";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 6;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void manyCustomerDelimiters2_squareBrackets_shouldAddAll() throws Exception {
        // given
        String numbers = "//[_-_][mm]\n1_-_2mm3";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 6;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void manyCustomDelimiterSpecialCharacters_squareBrackets_shouldAddAll() throws Exception {
        // given
        String numbers = "//[*][%]\n1*2%3%1";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 7;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void manyCustomDelimiterSpecialCharacters_squareBrackets_multipleSignsInDelimiter_shouldAddAll() throws Exception {
        // given
        String numbers = "//[*****][%%%%]\n1*****2%%%%3%%%%1";

        // when
        int result = sc.add(numbers);

        // then
        int expected = 7;
        Assert.assertEquals(expected, result);
    }
}