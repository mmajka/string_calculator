package mmajka;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringCalculator {

    private final static int MAX_NUM_VALUE = 1000;
    private final static String CUSTOM_DELIMITERS_REGEX = "//(.*?)\n";

    public int add(String numbers) throws Exception {

        if (numbers == null || numbers.isEmpty()) {
            return 0;
        }

        Matcher matcher = Pattern.compile(CUSTOM_DELIMITERS_REGEX).matcher(numbers);
        String splitRegex = prepareSplitRegex(matcher);

        if (matcher.find(0)) {
            numbers = removeCustomDelimitersString(numbers);
        }

        String[] numbersArray = numbers.split(splitRegex);
        return add(numbersArray);
    }

    private String prepareSplitRegex(Matcher matcher) {
        String splitRegex = ",|\n";
        if (matcher.find(0)) {
            splitRegex = matcher.group(1);
            List<String> delimiters = new ArrayList<>();
            Matcher subMatcher = Pattern.compile("\\[(.*?)\\]").matcher(splitRegex);
            while (subMatcher.find()) {
                delimiters.add(Pattern.quote(subMatcher.group(1)));
            }

            if (!delimiters.isEmpty()) {
                splitRegex = String.join("|", delimiters);
            }
        }
        return splitRegex;
    }

    private String removeCustomDelimitersString(String numbers) {
        return numbers.replaceFirst(CUSTOM_DELIMITERS_REGEX, "");
    }

    private int add(String[] numbersArray) throws Exception {
        int result = 0;
        List<Integer> negatives = new ArrayList<>();
        for (String numString : numbersArray) {
            int num = Integer.parseInt(numString);
            if (num < 0) {
                negatives.add(num);
            }
            if (num > 0 && num < MAX_NUM_VALUE) {
                result += num;
            }
        }

        if (!negatives.isEmpty()) {
            throw new Exception(String.format("Negatives not allowed:  %s", negatives.toString()));
        }
        return result;
    }
}
